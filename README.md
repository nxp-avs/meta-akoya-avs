# meta-akoya-avs (AVS BSP for i.MX7D Akoya platform)
---

Recipes to include Amazon's Alexa Voice Services on i.MX7D Akoya Platform
---
### Step 1 : Get iMX Yocto AVS setup environment

Review the instructions under Chapter 3 of the **i.MX_Yocto_Project_User's_Guide.pdf** on the [L4.X.X_X.X.X_LINUX_DOCS](https://www.nxp.com/webapp/Download?colCode=L4.1.15_1.0.0_LINUX_DOCS&Parent_nodeId=1276810298241720831102&Parent_pageType=product) to prepare your host machine. Including at least the following essential Yocto packages

	$ sudo apt-get install gawk wget git-core diffstat unzip texinfo gcc-multilib \
      build-essential chrpath socat libsdl1.2-dev u-boot-tools

#### Install the i.MX NXP AVS repo

Create/Move to a directory where you want to install the AVS yocto build enviroment.
Let's call this as <yocto_dir>

	$ cd <yocto_dir>
	$ repo init -u https://nxp-avs@bitbucket.org/nxp-avs/meta-akoya-avs.git -b master -m akoya-avs-sdk_1.2.1.xml

#### Download the AVS BSP build environment:

	$ repo sync

---
### Step 2: Run i.MX NXP Yocto Setup for Akoya's Alexa_SDK image using akoya setup script:

Run the **akoya-avs-setup-demo.sh** script as follows to setup your environment for the imx7d-pico board:

	$ MACHINE=imx7d-pico DISTRO=fsl-imx-x11 source akoya-avs-setup-demo.sh -b <build_sdk>

Where <build_sdk> is the name you will give to your build folder.

After acepting the EULA the script will prompt if you want to enable:

#### WIFI

	Enable WIFI for imx7d-pic board[Y/N]? y


**NOTE:** You should get the WiFi firmware binaries from Technexion. Otherwise select N (Check Appendix A for more details on how to install the Wifi Firmware from the build)

**Reacall that Wifi/BT packages are optional, since AVS/Alexa can work with an Ethernet conexion too.**

#### Finish avs-image configuration

At the end, you will see a text according with the configuration you select for your image build.

	==========================================================
	 AVS configuration is now ready at conf/local.conf

	 - WiFi/BT = Disabled
	 - Sound Card = Akoya Sound Card

	 You are ready to bitbake your AVS demo image now:

	    bitbake avs-image

	==========================================================


---

### Step 3: Get the extra packages
Due to license restrictions, you should get the following packages from its creators:

* **firmware-bcmdhd** Contact [Technexion](https://www.technexion.com/support/) FAE or Sales to get licensed firmware files.


* **Amazon's Pryon WakeWord resources and libraries**

	* akoya_sample_app_sdk_1.2.1.zip (Contact Amazon or NXP)

* **FFK Sound Card firmware**

	* amzn-dsp-spi.fw (Contact Amazon or NXP)

* **AVS daemons package for imx7d-pico board**

	* pico7d-daemons.zip (Contact Amazon or NXP)

---

### Step 4: Copy the extra packages to your yocto downloads directory


if there is no downloads directory (at this point yet) just create it on your <yocto_dir> directory

	$ cd <yocto_dir>
	$ mkdir downloads


It should look like this:

	$ ls <yocto_dir>
	avs.conf                 build_avs   fsl-setup-release.sh  README-IMXBSP  sources
	akoya-avs-setup-demo.sh  downloads   README


Copy the Akoya packages (if you reply YES to enable AVS/Alexa packages on Step 2):

	$ cp -v akoya_sample_app_sdk_1.2.1.zip <yocto_dir>/downloads
	$ cp -v amzn-dsp-spi.fw <yocto_dir>/downloads
	$ cp -v pico7d-daemons.zip <yocto_dir>/downloads


Copy the firmware-bcmhd files to your downloads directory (If enabled on Step 2):

	$ cp -v  bcm4339a0.hcd <yocto_dir>/downloads
	$ cp -v  bcm43438a0.hcd <yocto_dir>/downloads
	$ cp -v  fw_bcm4339a0_ag_apsta.bin <yocto_dir>/downloads
	$ cp -v  fw_bcm4339a0_ag.bin <yocto_dir>/downloads
	$ cp -v  fw_bcm4339a0_ag_mfg.bin <yocto_dir>/downloads
	$ cp -v  fw_bcm43438a0_apsta.bin <yocto_dir>/downloads
	$ cp -v  fw_bcm43438a0.bin <yocto_dir>/downloads
	$ cp -v  fw_bcm43438a0_mfg.bin <yocto_dir>/downloads
	$ cp -v  nvram_ap6212.txt <yocto_dir>/downloads
	$ cp -v  nvram_ap6335.txt <yocto_dir>/downloads

**Wifi packages are optional, since AVS/Alexa can work with an Ethernet conexion too.**



### Step 5: Build the AVS image

Go to your <build_sdk> directory and start the build of the avs-image

    $ cd  <yocto_dir>/<build_sdk>
    $ bitbake avs-image
---

### Step 6 : Deploying the built images to SD/MMC card to boot on target board.

After a build has succesfully completed, the created image resides at

    <build_sdk>/tmp/deploy/images/imx7d-pico/

In this directory, you will find the **imx7d-pico-avs.sdcard** image.

To Flash the .sdcard image into the eMMC device of your PicoPi board follow the next steps:

- Download the [bootbomb flasher](ftp://ftp.technexion.net/development_resources/development_tools/installer/pico-imx7-imx6ul-imx6ull_otg-installer_20170112.zip) and follow the instruction on **Section 4. Board Reflashing** of the [Quick Start Guide for AVS kit](https://www.nxp.com/docs/en/user-guide/Quick-Start-Guide-for-Arrow-AVS-kit.pdf) to setup your board on flashing mode.

- Copy the built SDCARD file

		$ sudo dd if=imx7d-pico-avs.sdcard of=/dev/sd<partition> bs=1M && sync
		$ sync

- Properly eject the pico-imx7d board:

		$ sudo eject /dev/sd<partition>

---

### NXP Documentation

---
### Appendix A. How to install Wifi firmware on the image

**NOTE:** If you enable the Wifi on you setup.

* **firmware-bcmdhd** Contact [Technexion](https://www.technexion.com/support/) FAE or Sales to get licensed firmware files.


## Copy the Wifi/BT firmware binaries to your yocto downloads directory

Before building (bitbake) the image you should include the firmware binaries into the build.
if there is no downloads directory (at this point yet) just create it on your <yocto_dir> directory

	$ cd <yocto_dir>
	$ mkdir downloads

It should look like this:

	$ ls <yocto_dir>
	avs.conf           build_sdk  fsl-setup-release.sh  README-IMXBSP      sources
	avs-setup-demo.sh  downloads   README


Copy the firmware-bcmhd files to your downloads directory (If enabled on Step 2):

	$ cp -v  bcm4339a0.hcd <yocto_dir>/downloads
	$ cp -v  bcm43438a0.hcd <yocto_dir>/downloads
	$ cp -v  fw_bcm4339a0_ag_apsta.bin <yocto_dir>/downloads
	$ cp -v  fw_bcm4339a0_ag.bin <yocto_dir>/downloads
	$ cp -v  fw_bcm4339a0_ag_mfg.bin <yocto_dir>/downloads
	$ cp -v  fw_bcm43438a0_apsta.bin <yocto_dir>/downloads
	$ cp -v  fw_bcm43438a0.bin <yocto_dir>/downloads
	$ cp -v  fw_bcm43438a0_mfg.bin <yocto_dir>/downloads
	$ cp -v  nvram_ap6212.txt <yocto_dir>/downloads
	$ cp -v  nvram_ap6335.txt <yocto_dir>/downloads

