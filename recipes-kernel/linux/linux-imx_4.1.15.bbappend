# Copyright (C) 2017 NXP Semiconductors
# Released under the MIT license (see COPYING.MIT for the terms)

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-${PV}:"

SRC_URI_append_imx7d-pico-ffk += " \
		file://0001-Adding-Amazon-AVS-device-tree-changes-for-pico-imx7d.patch \
		file://0002-Adding-FFK-DSP-driver-enabling-DSP-firmware-download.patch \
		file://0003-Adding-ffk-driver-enabling-playback-functionlity-ove.patch \
		file://0004-firmware-amzn-dsp-spi.fw-installed-from-userland.patch \
"

COMPATIBLE_MACHINE = "(mx7)"
