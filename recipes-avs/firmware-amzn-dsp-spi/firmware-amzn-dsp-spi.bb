SUMMARY = "Amazon DSP SPI firmware"
DESCRIPTION = "Amazon DSP SPI firmware"
SECTION = "base"
LICENSE_FLAGS = "alexa_materials"
LICENSE = "Alexa_Voice_Service_Agreement"
LIC_FILES_CHKSUM = "file://${WORKDIR}/LICENSE;md5=1df51444d06840a8cca7a77661b2390d"

SRC_URI += " \
    file://LICENSE \
    file://amzn-dsp-spi.fw \
"

S = "${WORKDIR}"

do_install() {
    install -d ${D}/lib/firmware/
    install -m 0755 amzn-dsp-spi.fw ${D}/lib/firmware/
}

FILES_${PN} += "/lib/firmware/"

COMPATIBLE_MACHINE = "(mx6|mx6ul|mx7)"
