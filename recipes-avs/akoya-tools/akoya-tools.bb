DESCRIPTION = "Custom scripts for start the AVS daemons and application and other utilities for imx7d-pico board"
HOMEPAGE = "http://developer.amazon.com"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"
S = "${WORKDIR}"
BB_NUMBER_THREADS = "1"

DEST_SCRIPTS_DIR ?= "/home/root/Alexa_SDK/Scripts"

SRC_URI = "file://init-functions \
		   file://vars.sh \
		   file://eth_start.sh \
		   file://wifi_start.sh \
           file://startDaemons.sh \
           file://stopDaemons.sh \
           file://runAkoyaSample.sh \
           file://no_sensory \
"

do_install() {
    install -d -m 0755 ${D}/lib/init
    install -d -m 0755 ${D}/lib/lsb
    install -d -m 0755 ${D}/etc/alexa_sdk
    install -d -m 0755 ${D}${DEST_SCRIPTS_DIR}
    install ${S}/init-functions ${D}/lib/lsb
    install ${S}/vars.sh ${D}/lib/init
    install ${S}/*.sh ${D}${DEST_SCRIPTS_DIR}
    install ${S}/no_sensory ${D}/etc/alexa_sdk/no_sensory

}

FILES_${PN} = "${DEST_SCRIPTS_DIR}/ /lib/init /lib/lsb /etc/alexa_sdk"
BBCLASSEXTEND = "native"
