DESCRIPTION = "Amazon binaries and daemons for AVS "
HOMEPAGE = "http://developer.amazon.com"
SECTION = "devel"
LICENSE_FLAGS = "alexa_materials"
LICENSE = "Alexa_Voice_Service_Agreement"
LIC_FILES_CHKSUM = "file://${WORKDIR}/LICENSE;md5=a112785aece665ab665114097c70ed86"
S = "${WORKDIR}/${PN}"
BB_NUMBER_THREADS = "1"

DEST_DIR ?= "/opt/alexa/samples"

DEPENDS = "boost"

SRC_URI = "file://pico7d-daemons.zip;subdir=${PN} \
		   file://LICENSE \
"
INSANE_SKIP_${PN} = "already-stripped"
INSANE_SKIP_${PN} += "rpaths"
INSANE_SKIP_${PN} += "debug-files"
INSANE_SKIP_${PN} += "ldflags"
INSANE_SKIP_${PN} += "staticdev"

do_install() {
    install -d -m 0755 ${D}/usr/etc/ledd
    install -d -m 0755 ${D}/usr/bin
    install -d -m 0755 ${D}/etc/init.d
    install ${S}/akoya_ledd/config/* ${D}/usr/etc/ledd/
    install ${S}/akoya_controld/control_daemon ${D}/usr/bin/
    install ${S}/akoya_ledd/ledd ${D}/usr/bin/
    install ${S}/akoya_buttond/button_daemon ${D}/usr/bin/
    install ${S}/akoya_controld/files/akoya_controld ${D}/etc/init.d/
    install ${S}/akoya_ledd/files/akoya_ledd ${D}/etc/init.d/
    install ${S}/akoya_buttond/files/akoya_buttond ${D}/etc/init.d/

}

FILES_${PN} = "${DEST_DIR} /usr/etc/ledd /usr/bin /etc/init.d"
BBCLASSEXTEND = "native"
